import json
import logging
from copy import deepcopy

import jsonschema
import six
from jsonschema import ValidationError, Draft4Validator, SchemaError
from jsonschema.exceptions import best_match

CLASS_FIELD = 'jsonClass'

def recursively_merge_json_schemas(a, b, json_path=""):
    # assert a.__class__ == b.__class__, str(a.__class__) + " vs " + str(b.__class__)

    if isinstance(b, dict) and isinstance(a, dict):
        #if 'type' in b:
        #    return deepcopy(b)

        a_and_b = set(a.keys()) & set(b.keys())
        every_key = set(a.keys()) | set(b.keys())
        merged_dict = {}
        for k in every_key:
            if k in a_and_b:
                if k.startswith('_') and k != '_display':
                    merged_dict[k] = deepcopy(b[k])
                    continue
                merged_dict[k] = recursively_merge_json_schemas(a[k], b[k], json_path=json_path + "/" + k)
            else:
                if k in ['anyOf', 'oneOf'] and k in a and k not in b:
                    continue
                merged_dict[k] = deepcopy(a[k] if k in a else b[k])
        return merged_dict
    elif (isinstance(b, list)
            and isinstance(a, list)
            and not (json_path.endswith(CLASS_FIELD + "/enum") or json_path.endswith('type' + "/enum"))):
        # TODO: What if we have a list of dicts?
        items_are_dicts = (len(a) and isinstance(a[0], dict)) or (len(b) and isinstance(b[0], dict))
        items_are_lists = (len(a) and isinstance(a[0], list)) or (len(b) and isinstance(b[0], list))
        return b if items_are_dicts else (a + b if items_are_lists else list(set(a + b)))
    else:
        return deepcopy(b)


class VVSchemas():
    def __init__(self):
        pass
        
    def get(self, schema_name):
        pass

    def get_ancestors(self, schema_name):
        pass

    def list(self):
        pass


class SchemaValidator(object):

    def __init__(self, vv_schemas, colln):
        self.vv_schemas = vv_schemas
        self.colln = colln
        self._vv_validator_cls = create_custom_validator(self.vv_schemas, self.colln)

    def _get_schema(self, doc):
        if 'jsonClass' not in doc:
            raise ValidationError('json_class is required field')
        schema = self.vv_schemas.get(doc['jsonClass'])
        if schema is None:
            raise ValidationError(
                'schema definition for jsonClass {} does not exist'.format(doc['jsonClass'])
            )
        return schema

    def __setattr__(self, key, value):
        super(SchemaValidator, self).__setattr__(key, value)
        if key in ('vvschemas', 'colln'):
            self._vv_validator_cls = create_custom_validator(self.vv_schemas, self.colln)

    def validate(self, doc, diff=False):
        schema = self._get_schema(doc).copy()
        if diff:
            schema.pop('required', None)
        vv_validator = self._vv_validator_cls(schema)

        try:
            vv_validator.validate(doc)
        except SchemaError as e:
            logging.error("Exception message: " + e.message)
            logging.error("Schema is: " + json.dumps(schema))
            logging.error("Context is: " + str(e.context))
            logging.error("Best match is: " + str(best_match(errors=[e])))
            raise e
        except ValidationError as e:
            logging.error("Exception message: " + e.message)
            logging.error("doc is: " + str(doc))
            logging.error("Schema is: " + json.dumps(schema, indent=2))
            logging.error("Context is: " + str(e.context))
            logging.error("Best match is: " + str(best_match(errors=[e])))
            raise e


def create_custom_validator(vv_schemas, colln):
    #  old_object_validator = None


    def _allowed_classes_validation_fn(validator, property_value, instance, schema):

        def _validate_allowed_classes(json_class, allowed_classes):
            if json_class in allowed_classes:
                return True

            if not vv_schemas:
                raise ValidationError('vv_schemas not defined', instance=instance, schema=schema)

            parent_classes = vv_schemas.get_ancestors(json_class)

            if not len(set(parent_classes).intersection(set(allowed_classes))):
                raise ValidationError(
                    '{} is not an allowed class'.format(json_class),
                    instance=instance, schema=schema
                )

        if not isinstance(property_value, list):
            raise jsonschema.SchemaError('allwoedClasses must be an array of strings')
        are_strings = False not in [isinstance(c, str) for c in property_value]
        if not are_strings:
            raise jsonschema.SchemaError('allowedClasses must be an array of strings')

        instance_type = type(instance)
        if instance_type == str:
            obj = colln.find({"_id": instance})
            if obj is None:
                raise ValidationError('resource with _id {} does not exist.'.format(instance))
            _validate_allowed_classes(obj['jsonClass'], property_value)

        elif instance_type == dict:
            if 'jsonClass' not in instance:
                raise ValidationError("invalid object; object should have jsonClass")
            _validate_allowed_classes(instance['jsonClass'], property_value)
            instance_schema = vv_schemas.get(instance['jsonClass'])
            instance_validator = VVValidator(instance_schema)
            instance_validator.validate(instance)  # TODO

    all_validators = dict(Draft4Validator.VALIDATORS)
    all_validators['allowedClasses'] = _allowed_classes_validation_fn
    VVValidator = jsonschema.validators.create(meta_schema=Draft4Validator.META_SCHEMA, validators=all_validators)
    return VVValidator
